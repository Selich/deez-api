const server = require("http").createServer();
const amqp = require("amqplib");
const { sign_socket_in, sign_socket_out, play_track, add_track_to_queue } = require("./event_handlers");
const courier = require("./courier");
const api = require("./api");
const _ = require("lodash");
const PORT = 3005;
const RMQ_URL = "amqp://localhost";
const ADD_USER_QUEUE = "add_user_queue";

const io = require("socket.io")(server, {
	path: "",
	serveClient: false,
	// below are engine.IO options
	pingInterval: 500,
	pingTimeout: 1000,
	cookie: false
});

const ROOM_JOIN = "ROOM_JOIN";
const ROOM_LEFT = "ROOM_LEFT";
const ADD_TO_QUEUE = "ADD_TO_QUEUE";
const PLAY_TRACK = "PLAY_TRACK";

server.listen(PORT, async () => {
	try {
		const conn = await amqp.connect(RMQ_URL);
		const ch = await conn.createChannel();
		await ch.assertQueue(ADD_USER_QUEUE);

		io.on("connect", async socket => {
			console.log("[Connect] socket.id: ", socket.id);
			socket.on(ROOM_JOIN, () => {
				sign_socket_in(socket);
			});

			socket.on(ROOM_LEFT, () => {
				sign_socket_out(socket);
			});

			socket.on(ADD_TO_QUEUE, ({ track_id, track_thumbnail, track_artist, track_name }) => {
				add_track_to_queue(socket, track_id, track_name, track_thumbnail, track_artist);
			});
			socket.on(PLAY_TRACK, ({ track_id }) => {
				play_track(socket, track_id);
			});
		});
	} catch (err) {
		console.error(err);
	}

	console.log(`listening on port ${PORT}`);
});
