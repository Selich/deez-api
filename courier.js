const {
	getAsync,
	hgetallAsync,
	zRangeAsync,
	hmsetAsync,
	sAddAsync,
	sRemAsync,
	sMembersAsync,
	delAsync
} = require("./redis");
const async = require("async");
const JOIN_ROOM = "JOIN_ROOM";

// So called "APP_STATE"
// - who is online, current track and its timestamp, queue

const ONLINE_SOCKETS = "ONLINE_SOCKETS";

// Track data are stored like this
// TRACK_${TRACK_ID}
// and these are redis hashes

function get_room_state() {
	return new Promise(async (resolve, reject) => {
		try {
			const response = {
				online_sockets: [],
				current_track_data: null,
				current_track_timestamp: null,
				queue: []
			};

			const online_sockets = await sMembersAsync(ONLINE_SOCKETS);
			console.log("[get_room_state] online_sockets: ", online_sockets);
			// ONLINE_SOCKETS
			response.online_sockets = online_sockets;

			const current_track_id = await getAsync("current_track");
			if (current_track_id) {
				console.log("[get_room_state] current_track id: ", current_track_id);
				const current_track_data = hgetallAsync(`TRACK_${current_track_id}`);
				console.log("[get_room_state] current_track_data: ", current_track_data);
				// CURRENT_TRACK
				response.current_track_data = current_track_data;
				const current_track_timestamp = await getAsync(`TRACK_${current_track_id}`);
				// CURRENT_TRACK_TIMESTAMP
				response.current_track_timestamp = current_track_timestamp;
			} else {
				console.log("[get_room_state] current_track_id is null or undefined ", current_track_id);
			}

			const queue_ids = await zRangeAsync("TRACK_QUEUE", 0, -1, true);
			console.log("[get_room_state] queue_ids ", queue_ids);
			const queue = await get_queue_with_track_data(queue_ids);
			console.log("[get_room_state] queue: ", queue);
			// QUEUE
			response.queue = queue;
			resolve(response);
		} catch (err) {
			reject(err);
			console.error("[get_room_state] err: ", err);
		}
	});
}

function get_queue_with_track_data(queue_ids) {
	console.log("gets fking called??");
	return new Promise(async (resolve, reject) => {
		try {
			console.log("get user data called");
			const fns = [];
			const providerFn = cb => {
				cb(null, { ind: 0, queue_ids });
			};

			fns.push(providerFn);

			const get_track_data = (data, cb) => {
				console.log("dejta ", data);
				console.log("cb ", cb);
				const { ind, queue_ids } = data;
				if (queue_ids[ind]) {
					let track_id = queue_ids[ind];
					hgetallAsync(`TRACK_${track_id}`)
						.then(track_data => {
							console.log("[get_queue_with_track_data] track_data: ", track_data);
							cb(null, { ind: ind + 1, queue_tracks: { [track_id]: track_data } });
						})
						.catch(err => {
							cb(err);
							reject(err);
						});
				}
			};

			for (let i = 0; i < queue_ids.length; i++) {
				fns.push(get_track_data);
			}

			async.waterfall(fns, (err, result) => {
				console.log("err ", err);
				console.log("result ", result);
				let queue_with_track_data = [];

				if (!err) {
					for (let key in result["users"]) {
						queue_with_track_data.push(result["queue_tracks"][key]);
					}
					resolve(queue_with_track_data);
				} else resolve(result);
			});
		} catch (err) {
			reject(err);
			console.error(err);
		}
	});
}

function socket_join(socket) {
	return new Promise(async (resolve, reject) => {
		try {
			console.log("[socket_join] socket.id: ", socket.id);
			await sAddAsync(ONLINE_SOCKETS, socket.id);
			const online = await sMembersAsync(ONLINE_SOCKETS);
			console.log("[socket_join] Online users", online);
			resolve(online);
		} catch (err) {
			reject(err);
			console.error(err);
		}
	});
}

function socket_left(socket) {
	return new Promise(async (resolve, reject) => {
		try {
			console.log("[socket_left] socket.id: ", socket.id);
			const removed_user = sRemAsync(ONLINE_SOCKETS, socket.id);
			console.log("[socket_left] removed_user: ", removed_user);
			const online = await sMembersAsync(ONLINE_SOCKETS);
			resolve(online);
		} catch (err) {
			reject(err);
			console.error(err);
		}
	});
}

module.exports = {
	socket_join,
	socket_left,
	get_room_state
};
