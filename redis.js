const redis = require("redis"),
	redisClient = redis.createClient();
const { promisify } = require("util");

const existsAync = promisify(redisClient.EXISTS).bind(redisClient);
const sAddAsync = promisify(redisClient.SADD).bind(redisClient);
const sRemAsync = promisify(redisClient.SREM).bind(redisClient);
const sMembersAsync = promisify(redisClient.SMEMBERS).bind(redisClient);
const getAsync = promisify(redisClient.GET).bind(redisClient);
const hmsetAsync = promisify(redisClient.HMSET).bind(redisClient);
const hincrbyAsync = promisify(redisClient.HINCRBY).bind(redisClient);
const hgetAsync = promisify(redisClient.HGET).bind(redisClient);
const hgetallAsync = promisify(redisClient.HGETALL).bind(redisClient);
const existsAsync = promisify(redisClient.EXISTS).bind(redisClient);
const delAsync = promisify(redisClient.DEL).bind(redisClient);
const hdelAsync = promisify(redisClient.HDEL).bind(redisClient);
const zAddAsync = promisify(redisClient.ZADD).bind(redisClient);
const zRangeAsync = promisify(redisClient.ZRANGE).bind(redisClient);
const zRankAsync = promisify(redisClient.ZRANK).bind(redisClient);

module.exports = {
	existsAync,
	getAsync,
	sAddAsync,
	sRemAsync,
	sMembersAsync,
	hmsetAsync,
	hgetAsync,
	hgetallAsync,
	hincrbyAsync,
	existsAsync,
	delAsync,
	hdelAsync,
	zAddAsync,
	zRangeAsync,
	zRankAsync
};
