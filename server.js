const express = require("express");
const axios = require("axios");
const app = express();

const port = "3080";
app.get("/search", async (req, res) => {
	const { q } = req.query;
	try {
		const r = await axios.get(`https://api.deezer.com/search?q=${q}`);
		console.log(r.data.data);
		return res.status(200).json(r.data.data);
	} catch (err) {
		console.log("err: ", err);
	}
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
