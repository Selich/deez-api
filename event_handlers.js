const courier = require("./courier");
const { sAddAsync, sMembersAsync, sRemAsync, zRankAsync, zAddAsync } = require("./redis");

// EVENTS
const ONLINE_SOCKETS = "ONLINE_SOCKETS";
const SOCKETS_UPDATED = "SOCKETS_UPDATED";
const ROOM_JOIN_SUCCESSFULL = "ROOM_JOIN_SUCCESSFULL";
const APP_ERROR = "APP_ERROR";

async function sign_socket_in(socket) {
	try {
		// Add socket to set
		const socket_added = await sAddAsync(ONLINE_SOCKETS, socket.id);
		console.log("[sign_socket_in] socket_added: ", socket_added);

		// Get online_ockets, queue, current_track_data, current_track_timestamp
		const room_state = await courier.get_room_state();
		console.log("[sign_socket_in] room_state: ", room_state);

		// Goes to the recently connected socket
		socket.emit(ROOM_JOIN_SUCCESSFULL, room_state);

		// Goes to all sockets except the recently connected one
		socket.broadcast.emit(SOCKETS_UPDATED, { online_sockets: room_state.online_sockets });
	} catch (err) {
		console.log("[sign_socket_in] app_error: ", err);
		socket.emit(APP_ERROR);
	}
}

async function sign_socket_out(socket) {
	try {
		const socket_removed = await sRemAsync(ONLINE_SOCKETS, socket.id);
		console.log("[sign_socket_out] socket_removed: ", socket_removed);

		const online_sockets = await sMembersAsync(ONLINE_SOCKETS);
		console.log("[sign_socket_out] online_sockets: ", online_sockets);

		socket.broadcast.emit(SOCKETS_UPDATED, { online_sockets });
	} catch (err) {
		console.log("[sign_socket_out] app_error: ", err);
		socket.emit(APP_ERROR);
	}
}

// async function play_track(socket, track_id) {
// 	try {
//         const
// 	} catch (err) {

//     }
// }

// add_track_to_queue(socket, track_id, track_name, track_thumbnail, track_artist);
async function add_track_to_queue(socket, track_id, track_name, track_thumbnail, track_artist) {
	try {
        const
	} catch (err) {
		console.log("[add_track_to_queue] app_error: ", err);
		socket.emit(APP_ERROR);
	}
}
module.exports = {
	sign_socket_in,
	sign_socket_out
};
